require 'watir'   #-webdriver
require 'page-object'
require 'page-object/page_factory'
require 'rspec/expectations'
require 'data_magic'

World(PageObject::PageFactory)

if ENV['HE\\ADLESS'] == 'true'
  require 'headless'

  headless = Headless.new
  headless.start

  at_exit do
    headless.destroy
  end
end
