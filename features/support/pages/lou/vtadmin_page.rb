class VTAdminPermission

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  require 'selenium-webdriver'


  #attr_accessor :txtusername,:txtpassword,:btnlogin

  def initialize(browser)
    @browser = browser
    @txtusername = @browser.text_field(:id => "txtUsername")
    @txtpassword = @browser.text_field(:id =>  "txtPassword")
    @btnlogin = @browser.button(:id =>  "btnLogin")
    @tabhome = @browser.a(:text => "Home")

    @link_homeMerchants = @browser.a(:text => "Merchants")
    @link_homeUsers = @browser.a(:text => "Users")
    @link_homePartitions = @browser.a(:text => "Partitions")


    @tabusers = @browser.a(:text => "Users")
    @linksearch = @browser.a(:href => "frmUsers.aspx")
    @spanusersearch = @browser.span(:text => "User Search")

    @tab_merchants = @browser.a(:text => "Merchants")
    @link_merchant_search = @browser.a(:text => "Search")
    @span_merchant_search = @browser.span(:text => "Merchant Search")

    @txt_gateway_id =@browser.text_field(:id => "ctl00_ContentPlaceHolder1_txtMerchantID")
    @table_mer_search = @browser.table(:id => "ctl00_ContentPlaceHolder2_ctl01_GridView1")

    @tab_SageExchange = @browser.a(:text => "Sage Exchange")
    @link_SageExchange = @browser.a(:href => "frmCorporations.aspx")
    @span_corp_search = @browser.span(:text => "Corporation Search")
    @txt_vtid = @browser.text_field(:id => "ctl00_ContentPlaceHolder1_txtVTID")
    @btn_se_search = @browser.button(:text => "Search")
    @table_corp_search = @browser.table(:id => "ctl00_ContentPlaceHolder2_gvSearchResutls")

    @txt_search_name = @browser.text_field(:id => "ctl00_ContentPlaceHolder1_txtName")
    @btnSearch= @browser.button(:id => "ctl00_ContentPlaceHolder1_btnSearch")
    @tableSearch = @browser.table(:id => "ctl00_ContentPlaceHolder2_ctrlwa_admin_users1_GridView1")
    @link_view = @browser.a(:text =>"View")
    @link_se_chaining = @browser.a(:text =>"Chaining")

    @txt_search_name_title = @browser.span(:id => "ctl00_ContentPlaceHolder2_lblTitle")
    @txt_search_fullname = @browser.text_field(:id => "ctl00_ContentPlaceHolder2_ctrlwa_admin_users1_FormView1_txtFullName")

    @table_profile_permission = @browser.table(:id =>"ctl00_ContentPlaceHolder2_ctrlwa_admin_users1_FormView1")
    @btn_save= @browser.button(:value =>  "Save")

    @link_logout = @browser.a(:href =>  "frmLogin.aspx")
  end


  def visit(url)
    @browser.goto url
  end


  def enterun(uname)
    sleep(5)
    @txtusername.wait_until_present
    if @txtusername.exists?
      @txtusername.set uname
    end
  end


  def enterpw(pwd)
   #sleep(5)
    @txtpassword.wait_until_present
    if @txtpassword.exists?
      @txtpassword.set pwd
    end
  end


  def clicklogin
    sleep(5)
    @btnlogin.wait_until_present
    if @btnlogin.exists?
      @btnlogin.click
    end
  end

  def verifyhometpage()
    sleep(5)
    @tabhome.wait_until_present
    expect(@tabhome.visible?).to be_truthy

  end

  def verify_page_title(title)
    expect(@browser.title).to eql(title)
  end

  def click_user_search
    @tabusers.fire_event("onmouseover") #hover
    @linksearch.click  #when_present.click
    sleep(3)
  end


  def verify_user_search
    @spanusersearch.wait_until_present
    expect(@spanusersearch.visible?).to be_truthy
  end

  def enter_search_name(s_name)
    @txt_search_name.set s_name
  end

  def click_search
    @btnSearch.click
  end

  def verify_search_result(s_result)
    if @tableSearch.exists?
     #puts @browser.table(:id => "ctl00_ContentPlaceHolder2_ctrlwa_admin_users1_GridView1")[0][1].text
     expect(@tableSearch[1][1].text).to eql(s_result)
    end
  end

  def click_view
    if @link_view.exists?
      @link_view.click
    end
  end

  def view_user_record
    if @txt_search_name_title.exists?
      expect(@txt_search_name_title.text).to eql("SMaddipati")
     # expect(@txt_search_fullname.text).to eql("Srivally Maddipati")
    end
  end

  #def unselect_permission
    #if @table_profile_permission.exists?
      #for row_num in 0..10   #10
          #if @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set?
           # @table_profile_permission.table(:index => 1)[row_num][1].checkbox.clear
         # end
          #if @table_profile_permission.table(:index => 1)[row_num][3].checkbox.exists? && @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set?
            #@table_profile_permission.table(:index => 1)[row_num][3].checkbox.clear
          #end
      #end
    #end
 # end

  def select_all_permission
    if @table_profile_permission.exists?
      for row_num in 0..10   #10
        if not @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set?
          puts "set"
           @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set
        end
        if @table_profile_permission.table(:index => 1)[row_num][3].checkbox.exists?
          if not @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set?
            puts "set1"
            @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set
          end
        end
      end
    end
  end

  def unselect_permission(table)
    data=table.hashes
    table.hashes.each do |row|

      if @table_profile_permission.exists?
        for row_num in 0..10
          if @table_profile_permission.table(:index => 1)[row_num][0].text.eql? row['Permission']
            if @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set?
              @table_profile_permission.table(:index => 1)[row_num][1].checkbox.clear
              break
            end
          end
          if @table_profile_permission.table(:index => 1)[row_num][3].checkbox.exists? && (@table_profile_permission.table(:index => 1)[row_num][2].text.eql? row['Permission'])
           if @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set?
              @table_profile_permission.table(:index => 1)[row_num][3].checkbox.clear
              break
            end
          end
        end
      end
    end
  end

  def select_permission(table)
    data=table.hashes
    table.hashes.each do |row|

      if @table_profile_permission.exists?
        for row_num in 0..10
          if @table_profile_permission.table(:index => 1)[row_num][0].text.eql? row['Permission']
            if not @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set?
              @table_profile_permission.table(:index => 1)[row_num][1].checkbox.set
              break
            end
          end
          if @table_profile_permission.table(:index => 1)[row_num][3].checkbox.exists? && (@table_profile_permission.table(:index => 1)[row_num][2].text.eql? row['Permission'])
            if not @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set?
              @table_profile_permission.table(:index => 1)[row_num][3].checkbox.set
              break
            end
          end
        end
      end
    end
  end


  def click_save
    @btn_save.click
  end

  def handle_alert
    # First alert
    if  @browser.alert.exists?
      @browser.alert.ok
      sleep(2)
    end
    #Second alert
    if  @browser.alert.exists?
      @browser.alert.ok
      sleep(2)
    end
  end

  def click_logout
    if @link_logout.exists?
      @link_logout.click

      #browser.quit
     # sleep(5)
      #@browser.close
      #sleep(5)

      #browser = Watir::Browser.new :firefox
     # @browser = browser

      #@browser = Watir::Browser.new :firefox
      #@browser.goto url

      #@browser.refresh
      sleep(3)
      #ready = @browser.ready_state.eql? "complete"
      #@browser.wait

    end
  end

  def verify_absence_user_search
    @tabusers.fire_event("onmouseover") #hover
    if not @linksearch.present?
      expect(@linksearch.present?).to eql(false)
    end
    sleep(2)
  end

  def verify_absence_SageExchange_search
    @tab_SageExchange.fire_event("onmouseover") #hover
    if not @link_SageExchange.present?
      expect(@link_SageExchange.present?).to eql(false)
    end
    sleep(2)
  end

  def verify_inactivMerchantLink
    if @link_homeMerchants.exists?
      @link_homeMerchants.click
      expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/frmHome.aspx")
    end

  end

  def verify_inactivPartitionsLink
    if @link_homePartitions.exists?
      @link_homePartitions.click
      expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/frmHome.aspx")
    end

  end

  def verify_inactivUserLink
    if @link_homeUsers.exists?
      @link_homeUsers.click
      expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/frmHome.aspx")
    end

  end

  def verify_inactive_merchant_search
    @tab_merchants.fire_event("onmouseover") #hover
    if @link_merchant_search.attribute('disabled') == true
      @link_merchant_search.click
      puts "Inactive Merchants hyper link"
      expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/frmHome.aspx")
    end
    sleep(3)
  end

  def modify_url(url_update)
    @browser.goto "https://qa.sageexchange.com/gatewayboarding/"+url_update+".aspx"
    sleep(3)
  end

  def verify_acess_deniedpage
    expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/frmBlank.aspx")
  end

  def verify_acess_permissionpage(pageurl)
    expect(@browser.url).to eql("https://qa.sageexchange.com/gatewayboarding/"+pageurl)
  end

  def click_browser_back
    @browser.back
  end

  def click_se_search
    @tab_SageExchange.fire_event("onmouseover")
    if @link_SageExchange.present?
      @link_SageExchange.click
    end
    sleep(2)
  end

  def enter_vtid_search(vtid)
    @txt_vtid.set vtid
    @btn_se_search.send_keys :enter
    sleep(2)
  end

  def verify_corp_result(corpaccount)
    @table_corp_search.wait_until_present
    expect(@table_corp_search[1][4].text).to eql (corpaccount)
  end

  def click_corp_record_view
    if @link_view.exists?
      @link_view.click
    end
  end

  def verify_absence_se_chaining
   expect(@link_se_chaining.present?).to eql(false)
  end

  def click_merchant_search
    @tab_merchants.fire_event("onmouseover") #hover
    @link_merchant_search.click
    sleep(3)
  end

  def verify_merchant_search
    @span_merchant_search.wait_until_present
    expect(@span_merchant_search.visible?).to be_truthy
  end

  def enter_gatewayid(gatewayid)
    @txt_gateway_id.wait_until_present
    if @txt_gateway_id.exists?
      @txt_gateway_id.set gatewayid
    end
  end

  def verify_mer_search_result(gatewayid)
    if @table_mer_search.exists?
      expect(@table_mer_search[1][0].text).to eql(gatewayid)
    end
  end

  def verify_corporation_search
    @span_corp_search.wait_until_present
    expect(@span_corp_search.visible?).to be_truthy
  end

  def enter_vtid(vtid)
    @txt_vtid.wait_until_present
    if @txt_vtid.exists?
      @txt_vtid.set vtid
    end
  end

  def verify_corp_search_result(corpemail)
    if @table_corp_search
      expect(@table_corp_search[1][4].text).to eql(corpemail)
    end
  end


  def clickBrowserBack()
    @browser.back
  end

  def verifyLoginPage_presence(loginUrl)
    sleep(5)
    #expect(@btnlogin.visible?).to be_truthy
    expect(@browser.url).to eql (loginUrl)
  end


end
