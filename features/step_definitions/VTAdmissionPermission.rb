require 'watir'

Given(/^I launch VTAdmin application "(.*)"$/) do |url|
  @vtadmin = VTAdminPermission.new(@browser)
  @vtadmin.visit(url)
end

When(/^Login to the application using credentials "(.*)" and "(.*)"$/) do |uName,pwd|
  @vtadmin.enterun(uName)
  @vtadmin.enterpw(pwd)
  @vtadmin.clicklogin
end


Then(/^I should see VTAdmin home page$/) do
  @vtadmin.verifyhometpage
end

Then(/^VTADMIN page title should be "(.*)"$/) do |title|
  @vtadmin.verify_page_title(title)
end

When(/^Select Users>Search option$/) do
  @vtadmin.click_user_search
end

Then(/^It should open User Search page$/) do
  @vtadmin.verify_user_search
end

Given(/^Enter the search user name as "(.*)"$/) do |s_name|
  @vtadmin.enter_search_name(s_name)
end

When(/^Click on Search$/) do
  @vtadmin.click_search
end


Then(/^It should list the respective search result i.e "(.*)"$/) do|s_result|
  @vtadmin.verify_search_result(s_result)
end

When(/^Click on View button$/) do
  @vtadmin.click_view
end

Then(/^Respective user record details should be displayed$/) do
  @vtadmin.view_user_record
end

#Given(/^Unselect all the permissions for the user$/) do
  #@vtadmin.unselect_permission
#end

When(/^Click on Save$/) do
  @vtadmin.click_save
end

Then(/^Information saved popup should be displayed$/) do
  @vtadmin.handle_alert
end


Given(/^Logout from the VTAdmin application$/) do
  @vtadmin.click_logout
end


Then(/^Users>Search option should not be listed$/) do
  @vtadmin.verify_absence_user_search
end

Then(/^Merchants hyperlink present at the bottom of the page should not be clickable$/) do
  @vtadmin.verify_inactivMerchantLink
end

Then(/^Partitions hyperlink present at the bottom of the page should not be clickable$/) do
  @vtadmin.verify_inactivPartitionsLink
end

Then (/^Merchants>Search option should not be active$/)do
  @vtadmin.verify_inactive_merchant_search
end

When(/^Modify the URL to access (.*) option by passing "(.*)"$/) do |page,url_update|
  @vtadmin.modify_url(url_update)
end

Then(/^It should not open (.*) page$/) do|pagename|
 @vtadmin.verify_acess_deniedpage
end

Then(/^It should open (.*) page i.e "(.*)"$/) do|pagename,pageurl|
  @vtadmin.verify_acess_permissionpage(pageurl)
end

When(/^Click on browser back button$/) do
  @vtadmin.click_browser_back()
end

Given(/Select all the permissions for the user$/) do
  @vtadmin.select_all_permission
end

Given(/Unselect the permissions for the user$/) do|table|
  @vtadmin.unselect_permission(table)
end

Given(/Select the permissions for the user$/) do|table|
  @vtadmin.select_permission(table)
end

Then(/^Users hyperlink present at the bottom of the page should not be clickable$/) do
  @vtadmin.verify_inactivUserLink
end

Then(/^SageExchange>Search option should not be listed$/) do
  @vtadmin.verify_absence_SageExchange_search
end

Given(/^Select SageExchange>Search option$/) do
  @vtadmin.click_se_search
end

When(/^Enter VTID as (.*) and Click on Search button/) do |vtid|
  @vtadmin.enter_vtid_search(vtid)
end

Then(/^Corportation Search page should be displayed for "(.*)"$/) do|corpaccount|
  @vtadmin.verify_corp_result(corpaccount)
end

When(/^Click on View link$/) do
  @vtadmin.click_corp_record_view
end

Then(/^In corporation record detail page Chaining link should not be listed$/) do
  @vtadmin.verify_absence_se_chaining
end

When(/^Select Merchants>Search option$/) do
  @vtadmin.click_merchant_search
end

Then(/^It should open Merchant Search page$/) do
  @vtadmin.verify_merchant_search
end

Given(/^Enter the gateway id "(.*)"$/)do |gatewayid|
  @vtadmin.enter_gatewayid(gatewayid)
end

Given(/^It should list the repective "(.*)" merchant search result$/) do|gatewayid|
  @vtadmin.verify_mer_search_result(gatewayid)
end

Then(/^It should open Corportaion Search page$/) do
  @vtadmin.verify_corporation_search
end

Given(/^Enter the VT id "(.*)"$/)do |vtid|
  @vtadmin.enter_vtid(vtid)
end

Given(/^It should list the repective "(.*)" corporation search result$/) do|corpemail|
  @vtadmin.verify_corp_search_result(corpemail)
end

When(/^Browser back button is clicked$/) do
  @vtadmin.clickBrowserBack()
end

Then(/^Verify it remains in the login screen i.e (.*)$/)do|loginUrl|
  @vtadmin.verifyLoginPage_presence(loginUrl)
end














