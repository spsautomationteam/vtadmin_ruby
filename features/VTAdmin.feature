Feature: VT Admin application  functionality check
  I want to login to VTAdmin application and remove the permission of another user and
  then when the particular user access the application,their permissions are removed
  so that can verify the Permission functionality

  @verify_VTADMIN_PageTitle
  Scenario: Verifying  VTADMIN page title
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    And VTADMIN page title should be "Gateway Administration"

  @verify_Merchant_Permission
  Scenario: Verifying  VTADMIN Merchant permissions
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Users>Search option
    Then It should open User Search page
    Given Enter the search user name as "Srivally Maddipati"
    When Click on Search
    Then It should list the respective search result i.e "Srivally Maddipati"
    When Click on View button
    Then Respective user record details should be displayed
    Given Unselect the permissions for the user
      |Permission                    |
      |View Merchant                 |
      |View Virtual Check Terminal   |
      |Update Merchant               |
      |Update Virtual Check Terminal |
      |Add Merchants                 |
      |View Batch Reporting          |
      |View Virtual Applications     |
      |Update Virtual Applications   |
      |Batch Management              |
      |Update Risk                   |
    And Select the permissions for the user
      |Permission                    |
      |View Bankcard Terminal        |
      |View Change Logs              |
      |Update Bankcard Terminal      |
      |Add Users                     |
      |View Sage Exchange            |
      |Sage Exchange Chaining        |
    When Click on Save
    Then Information saved popup should be displayed
    Given Logout from the VTAdmin application
    When Login to the application using credentials "Smaddipati" and "Sage123456@"
    Then I should see VTAdmin home page
    And Merchants hyperlink present at the bottom of the page should not be clickable
    And Merchants>Search option should not be active
    When Modify the URL to access merchants_search option by passing "frmMerchants"
    Then It should not open Merchants_Search page
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access merchants_AddNew option by passing "frmMerchantWizard"
    Then It should not open NewMerchants_Wizard page

  @verify_User_Permission
  Scenario: Verifying  VTADMIN User permissions
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Users>Search option
    Then It should open User Search page
    Given Enter the search user name as "Srivally Maddipati"
    When Click on Search
    Then It should list the respective search result i.e "Srivally Maddipati"
    When Click on View button
    Then Respective user record details should be displayed
    Given Unselect the permissions for the user
      |Permission               |
      |Add Users                |
    And Select the permissions for the user
      |Permission                    |
      |View Merchant                 |
      |View Virtual Check Terminal   |
      |Update Merchant               |
      |Update Virtual Check Terminal |
      |Add Merchants                 |
      |View Batch Reporting          |
      |View Virtual Applications     |
      |Update Virtual Applications   |
      |Batch Management              |
      |Update Risk                   |
      |View Bankcard Terminal        |
      |View Change Logs              |
      |Update Bankcard Terminal      |
      |View Sage Exchange            |
      |Sage Exchange Chaining        |
    When Click on Save
    Then Information saved popup should be displayed
    Given Logout from the VTAdmin application
    When Login to the application using credentials "Smaddipati" and "Sage123456@"
    Then I should see VTAdmin home page
    And Users hyperlink present at the bottom of the page should not be clickable
    And Users>Search option should not be listed
    When Modify the URL to access users_AddNew option by passing "frmUserWizard"
    Then It should not open New_User_Wizard page
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access support_DevNew option by passing "frmDevConnections"
    Then It should not open DevConnections_Wizard page
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access support_VPTLogs option by passing "frmVPTLogs"
    Then It should not open VPT_Logs page
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access support_EmailNotification option by passing "frmEmailNotification"
    Then It should not open Email_Notification_wizard page

  @verify_SageExchange_Permission
  Scenario: Verifying  SageExchange permissions
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Users>Search option
    Then It should open User Search page
    Given Enter the search user name as "Srivally Maddipati"
    When Click on Search
    Then It should list the respective search result i.e "Srivally Maddipati"
    When Click on View button
    Then Respective user record details should be displayed
    Given Unselect the permissions for the user
      |Permission               |
      |View Sage Exchange       |
      |Sage Exchange Chaining   |
    And Select the permissions for the user
      |Permission                    |
      |View Merchant                 |
      |View Virtual Check Terminal   |
      |Update Merchant               |
      |Update Virtual Check Terminal |
      |Add Merchants                 |
      |View Batch Reporting          |
      |View Virtual Applications     |
      |Update Virtual Applications   |
      |Batch Management              |
      |Update Risk                   |
      |View Bankcard Terminal        |
      |View Change Logs              |
      |Update Bankcard Terminal      |
      |Add Users                     |
    When Click on Save
    Then Information saved popup should be displayed
    Given Logout from the VTAdmin application
    When Login to the application using credentials "Smaddipati" and "Sage123456@"
    Then I should see VTAdmin home page
    And SageExchange>Search option should not be listed
    When Modify the URL to access SageExchange_Search option by passing "frmCorporations"
    Then It should not open Corporation_Search page
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access SageExchange_CorpRecord option by passing "frmCorporationsRecord"
    Then It should not open Corporation_Record page

  @verify_SageExchange_chaining_Permission
  Scenario: Verifying  SageExchange Chaining permissions
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Users>Search option
    Then It should open User Search page
    Given Enter the search user name as "Srivally Maddipati"
    When Click on Search
    Then It should list the respective search result i.e "Srivally Maddipati"
    When Click on View button
    Then Respective user record details should be displayed
    Given Unselect the permissions for the user
        |Permission               |
        |Sage Exchange Chaining   |
    And Select the permissions for the user
      |Permission                    |
      |View Merchant                 |
      |View Virtual Check Terminal   |
      |Update Merchant               |
      |Update Virtual Check Terminal |
      |Add Merchants                 |
      |View Batch Reporting          |
      |View Virtual Applications     |
      |Update Virtual Applications   |
      |Batch Management              |
      |Update Risk                   |
      |View Bankcard Terminal        |
      |View Change Logs              |
      |Update Bankcard Terminal      |
      |Add Users                     |
      |View Sage Exchange            |
    When Click on Save
    Then Information saved popup should be displayed
    Given Logout from the VTAdmin application
    When Login to the application using credentials "Smaddipati" and "SageSage123456@"
    Then I should see VTAdmin home page
    Given Select SageExchange>Search option
    When Enter VTID as 999999999997 and Click on Search button
    Then Corportation Search page should be displayed for "itqa@sage.com"
    When Click on View link
    Then In corporation record detail page Chaining link should not be listed


  @verify_Partitions_Permission
  Scenario: Verifying  VTADMIN partition permissions
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Users>Search option
    Then It should open User Search page
    Given Enter the search user name as "Srivally Maddipati"
    When Click on Search
    Then It should list the respective search result i.e "Srivally Maddipati"
    When Click on View button
    Then Respective user record details should be displayed
    Given Unselect the permissions for the user
      |Permission                    |
      |View Merchant                 |
      |View Virtual Check Terminal   |
      |Update Merchant               |
      |Update Virtual Check Terminal |
      |Add Merchants                 |
      |View Batch Reporting          |
      |View Virtual Applications     |
      |Update Virtual Applications   |
      |Batch Management              |
      |Update Risk                   |
      |View Bankcard Terminal        |
      |View Change Logs              |
      |Update Bankcard Terminal      |
      |Add Users                     |
      |View Sage Exchange            |
      |Sage Exchange Chaining        |
    When Click on Save
    Then Information saved popup should be displayed
    Given Logout from the VTAdmin application
    When Login to the application using credentials "Smaddipati" and "SageSage123456@"
    Then I should see VTAdmin home page
    And Partitions hyperlink present at the bottom of the page should not be clickable
    When Modify the URL to access partitions_search option by passing "frmProcessors"
    Then It should open partition_Search page i.e "frmProcessors.aspx"
    When Click on browser back button
    Then I should see VTAdmin home page
    When Modify the URL to access partitions_AddNew option by passing "frmProcessorWizard"
    Then It should open NewPartition_Wizard page i.e "frmProcessorWizard.aspx"


  @verify_VTADMIN_virtual_application_login
  Scenario: Verifying  VTADMIN virtual application login
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Spallepati" and "Sage!234"
    Then I should see VTAdmin home page
    When Select Merchants>Search option
    Then It should open Merchant Search page
    Given Enter the gateway id "999999999997"
    When Click on Search
    Then It should list the repective "999999999997" merchant search result


  @verify_VTADMIN_SageExchange_application_login
  Scenario: Verifying  VTADMIN SageExchange application login
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Smaddipati" and "Sage123456@"
    Then I should see VTAdmin home page
    When Select SageExchange>Search option
    And Enter VTID as 999999999997 and Click on Search button
    Then Corportation Search page should be displayed for "itqa@sage.com"

  @verify_VTADMIN_UserSession_AfterLogout
  Scenario: Verifying VTADMIN User session available after logging out
    Given I launch VTAdmin application "https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx"
    When Login to the application using credentials "Smaddipati" and "Sage123456@"
    Then I should see VTAdmin home page
    Given Logout from the VTAdmin application
    When Browser back button is clicked
    Then Verify it remains in the login screen i.e https://qa.sageexchange.com/gatewayboarding/frmLogin.aspx




